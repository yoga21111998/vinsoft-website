import React from 'react'
import Logo from '../assets/images/logo.png'
import facebook from '../assets/images/facebook.png'
import instagram from '../assets/images/insta.png'
import google from '../assets/images/google.png'
import linkedin from '../assets/images/linkedin.png'
import white_web from '../assets/images/white_web.png'
import '../assets/scss/footer.scss'
const footer = () => {

    return (
        <>
            <div className='footer'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-3 p-0'>
                            <img src={Logo}></img>
                            <div className='footer_para'>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi</p>
                            <p>Follow us on</p>
                            </div>
                            <div className='footer_logo'>
                                <img src={facebook} className='fb_img'></img>
                                <img src={google} className='google_img'></img>
                                <img src={linkedin} className='linked_img'></img>
                                <img src={instagram} className='insta_img'></img>
                            </div>
                        </div>
                        <div className='col-md-3'>
                            <div className='footer_address'>
                                <h2>Location</h2>
                                <h1>INDIA (Registered Office)</h1>
                                <p>Hysas technologies private Limited, No.61A1 Bolten Puram, 3rd street,</p>
                                <p>Thoothukudi - 628003.</p>
                                <p>Email: Info@hysas.com </p>
                                <p>Tel: +91-637 929 6448</p>
                            </div>
                        </div>
                        <div className='col-md-3'>
                            <div className='footer_second_address'>
                                <h2>Location</h2>
                                <h1>INDIA (Development Center)</h1>
                                <p>Hysas technologies private Limited, No.17/2 Visalatchi Nagar, 2nd street,</p>
                                <p>Ekkaduthangal Chennai - 600032.</p>
                                <p>Email: Info@hysas.com</p>
                            </div>
                        </div>
                        <div className='col-md-3'>
                            <div className='footer_submit'>
                            <img src={white_web} className='web_img'></img>
                                <input type="text" placeholder='Subscribe with us'></input>
                                <button type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default footer;