import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import services from '../assets/images/service_background.png'
import it from '../assets/images/it.png'
import web from '../assets/images/web.png'
import cyber from '../assets/images/cyber.png'
import about from '../assets/images/about.png'
import person from '../assets/images/person.png'
import clients from '../assets/images/clients.png'
import projects from '../assets/images/projects.png'
import experts from '../assets/images/expert.png'
import posts from '../assets/images/post.png'
import website from '../assets/images/website.png'
import android from '../assets/images/android.png'
import apple from '../assets/images/apple.png'
import iot from '../assets/images/iot.png'
import process from '../assets/images/process.png'
import requirement from '../assets/images/requirement.png'
import solution from '../assets/images/solution.png'
import prototype from '../assets/images/prototype.png'
import it_managment from '../assets/images/it_management.png'
import fleet_management from '../assets/images/fleet_management.png'
import restaurant_management from '../assets/images/restaurant_management.png'
import phone from '../assets/images/phone.png'
import mail from '../assets/images/mail.png'
import location from '../assets/images/location.png'
import facebook from '../assets/images/facebook.png'
import instagram from '../assets/images/insta.png'
import google from '../assets/images/google.png'
import linkedin from '../assets/images/linkedin.png'
import black from '../assets/images/black_background.png'
import green from '../assets/images/green_background.png'
import orange from '../assets/images/orange_background.png'
import quotes from '../assets/images/quotes.png'
import white_web from '../assets/images/white_web.png'
import white_round from '../assets/images/white_round.png'
import blue_quotes from '../assets/images/blue_quotes.png'
import Footer from '../Pages/footer'
import '../assets/scss/home.scss'

const Home = () => {

    return (
        <>
            <div className='services'>
                <Carousel interval={3000} pause={false}>
                    <Carousel.Item>
                        <img src={white_web} className='web_img'></img>
                        <img className="d-block w-100" src={services} alt="First slide" />
                        <Carousel.Caption>
                            <p className='services_head'>BEST IT SOLUTION PROVIDER</p>
                            <h1>Excellent IT services for your success</h1>
                            <button className='basic_btn'>Explore More <span><FontAwesomeIcon icon={faArrowRight} /></span></button>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img src={white_web} className='web_img'></img>
                        <img className="d-block w-100" src={services} alt="Second slide" />
                        <Carousel.Caption>
                            <p className='services_head'>BEST IT SOLUTION PROVIDER</p>
                            <h1>Excellent IT services for your success</h1>
                            <button className='basic_btn'>Explore More <span><FontAwesomeIcon icon={faArrowRight} /></span></button>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img src={white_web} className='web_img'></img>
                        <img className="d-block w-100" src={services} alt="Third slide" />
                        <Carousel.Caption>
                            <p className='services_head'>BEST IT SOLUTION PROVIDER</p>
                            <h1>Excellent IT services for your success</h1>
                            <button className='basic_btn'>Explore More <span><FontAwesomeIcon icon={faArrowRight} /></span></button>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </div>
            <div className='container'>
                <div className='about_head'>
                    <button>View All Services<span><FontAwesomeIcon icon={faArrowRight} /></span></button>
                    <h2>WHAT WE OFFER</h2>
                    <h3>Excellent It services</h3>
                    <div className='row mt-5'>
                        <div className='col-md-4'>
                            <div className='card business'>
                                <div className='card'>
                                    <img src={it} className='img_it'></img>
                                </div>
                                <h3>IT Management</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='card business'>
                                <div className='card'>
                                    <img src={cyber} className='img_cyber'></img>
                                </div>
                                <h3>Cyber Security</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='card business'>
                                <div className='card'>
                                    <img src={web} className='img_web'></img>
                                </div>
                                <h3>Web Development</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div className='about_section'>
                <div className='container'>
                    <div className='row mt-5'>
                        <div className='col-md-6'>
                            <img src={white_web} className='web_img'></img>
                            <img src={about} className='about_img' alt="..." />
                        </div>
                        <div className='col-md-6'>
                            <h2>About Vinsoft</h2>
                            <h3>We strive to offer intelligent business solutions</h3>
                            <p>Morem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit amet feugiat lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            <div className="card_section">
                                <div className='card'>
                                    <img src={it}></img>
                                </div>
                                <div>
                                    <h1>Best services</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </div>
                                <div className='card'>
                                    <img src={person}></img>
                                </div>
                                <div>
                                    <h1>24/7 Call support</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </div>
                            </div>
                            <div>
                                <button>Explore More <span><FontAwesomeIcon icon={faArrowRight} /></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='about_image'>
                <div className='row'>
                    <div className='col-md-3 mt-5'>
                        <div className='rating'>
                            <div>
                                <img src={clients}></img>
                            </div>
                            <div>
                                <p className='rating_content'>6,561 +</p>
                                <p className='rating_content'>Satisfied Clients</p>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-3 mt-5'>
                        <div className='rating'>
                            <div >
                                <img src={projects}></img>
                            </div>
                            <div>
                                <p className='rating_content'>600 +</p>
                                <p className='rating_content'>Finished Projects</p>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-3 mt-5'>
                        <div className='rating'>
                            <div >
                                <img src={experts}></img>
                            </div>
                            <div>
                                <p className='rating_content'>100 +</p>
                                <p className='rating_content'>Skilled Experts</p>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-3 mt-5'>
                        <div className='rating'>
                            <div >
                                <img src={posts}></img>
                            </div>
                            <div>
                                <p className='rating_content'>100 +</p>
                                <p className='rating_content'>Media Posts</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container'>
                <div className='case_studies'>
                    <button>View All Case<span><FontAwesomeIcon icon={faArrowRight} /></span></button>
                    <h2>From our case studies</h2>
                    <h3>We Delivered Best Solution</h3>
                    <div className='container'>
                        <Carousel interval={3000} pause={false}>
                            <Carousel.Item>
                                <div className='row mt-5'>
                                    <div className='col-md-4'>
                                        <div className='card case'>
                                            <img className="d-block w-100 position-relative" src={it_managment} alt="Slide 1" />
                                            <h3>Solution</h3>
                                            <h4>IT Management</h4>
                                            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                        </div>
                                    </div>
                                    <div className='col-md-4'>
                                        <div className='card case'>
                                            <img className="d-block w-100 position-relative" src={fleet_management} alt="Slide 1" />
                                            <h3>Solution</h3>
                                            <h4>Fleet Management</h4>
                                            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                        </div>
                                    </div>
                                    <div className='col-md-4'>
                                        <div className='card case'>
                                            <img className="d-block w-100 position-relative" src={restaurant_management} alt="Slide 1" />
                                            <h3>Solution</h3>
                                            <h4>Restaurant Management</h4>
                                            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                        </div>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <div className='row mt-5'>
                                    <div className='col-md-4'>
                                        <div className='card case'>
                                            <img className="d-block w-100 position-relative" src={it_managment} alt="Slide 2" />
                                            <h3>Solution</h3>
                                            <h4>IT Management</h4>
                                            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                        </div>
                                    </div>
                                    <div className='col-md-4'>
                                        <div className='card case'>
                                            <img className="d-block w-100 position-relative" src={fleet_management} alt="Slide 2" />
                                            <h3>Solution</h3>
                                            <h4>Fleet Management</h4>
                                            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                        </div>
                                    </div>
                                    <div className='col-md-4'>
                                        <div className='card case'>
                                            <img className="d-block w-100 position-relative" src={restaurant_management} alt="Slide 2" />
                                            <h3>Solution</h3>
                                            <h4>Restaurant Management</h4>
                                            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                        </div>
                                    </div>
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <div className='row mt-5'>
                                    <div className='col-md-4'>
                                        <div className='card case'>
                                            <img className="d-block w-100 position-relative" src={it_managment} alt="Slide 2" />
                                            <h3>Solution</h3>
                                            <h4>IT Management</h4>
                                            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                        </div>
                                    </div>
                                    <div className='col-md-4'>
                                        <div className='card case'>
                                            <img className="d-block w-100 position-relative" src={fleet_management} alt="Slide 2" />
                                            <h3>Solution</h3>
                                            <h4>Fleet Management</h4>
                                            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                        </div>
                                    </div>
                                    <div className='col-md-4'>
                                        <div className='card case'>
                                            <img className="d-block w-100 position-relative" src={restaurant_management} alt="Slide 2" />
                                            <h3>Solution</h3>
                                            <h4>Restaurant Management</h4>
                                            <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                        </div>
                                    </div>
                                </div>
                            </Carousel.Item>
                        </Carousel>
                    </div>
                </div>
            </div>
            <div className='container'>
                <div className='offer'>
                    <button>Explore More<span><FontAwesomeIcon icon={faArrowRight} /></span></button>
                    <h2>Our offering</h2>
                    <h3>Enhance & Pioneer Using Technology Trends</h3>
                    <div>
                        <img src={website}></img>
                        <img src={android}></img>
                        <img src={apple}></img>
                        <img src={iot}></img>
                    </div>
                </div>
            </div>
            <div className='container'>
                <div className='development'>
                    <button>Explore More<span><FontAwesomeIcon icon={faArrowRight} /></span></button>
                    <h2>Work Process</h2>
                    <h3>Our Development Process</h3>
                    <img src={process} className='process_img'></img>
                    <div className='row mt-5'>
                        <div className='col-md-4'>
                            <div className='card development'>
                                <img src={requirement} className='img_it'></img>
                                <h3>Define Requirements</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='card development'>
                                <img src={prototype} className='img_cyber'></img>
                                <h3>Design & Prototyping</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='card development'>
                                <img src={solution} className='img_web'></img>
                                <h3>Final solution</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container'>
                <div className='review'>
                    <h2>Clients Review</h2>
                    <h3>What they say about our work</h3>
                    <div className='review_img'>
                        <div className='row mt-5'>
                            <div className='col-md-3'>
                                <img src={white_web} className='web_img'></img>
                                <img src={white_round} className='web_white_round_img'></img>
                                <img src={blue_quotes} className='web_blue_quotes_img'></img>
                                <p className='clients_content'>What clients have to say about us</p>
                            </div>
                            <div className='col-md-9'>
                                <Carousel interval={3000} pause={false}>
                                    <Carousel.Item>
                                        <div className='row mt-5'>
                                            <div className='col-md-4'>
                                                <div className='card review'>
                                                    <img className="d-block w-100 position-relative" src={black} alt="Slide 1" />
                                                    <div className='card review_logo'>
                                                        <p>Logo</p>
                                                        <img src={quotes}></img>
                                                    </div>
                                                    <div className='review_content'>
                                                        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                                        <h3>Alex Rony</h3>
                                                        <h4>Web Designer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-md-4'>
                                                <div className='card review'>
                                                    <img className="d-block w-100 position-relative" src={green} alt="Slide 1" />
                                                    <div className='card review_logo'>
                                                        <p>Logo</p>
                                                        <img src={quotes}></img>
                                                    </div>
                                                    <div className='review_content'>
                                                        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                                        <h3>Alex Rony</h3>
                                                        <h4>Web Designer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-md-4'>
                                                <div className='card review'>
                                                    <img className="d-block w-100 position-relative" src={orange} alt="Slide 1" />
                                                    <div className='card review_logo'>
                                                        <p>Logo</p>
                                                        <img src={quotes}></img>
                                                    </div>
                                                    <div className='review_content'>
                                                        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                                        <h3>Alex Rony</h3>
                                                        <h4>Web Designer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Carousel.Item>
                                    <Carousel.Item>
                                        <div className='row mt-5'>
                                            <div className='col-md-4'>
                                                <div className='card review'>
                                                    <img className="d-block w-100 position-relative" src={black} alt="Slide 1" />
                                                    <div className='card review_logo'>
                                                        <p>Logo</p>
                                                        <img src={quotes}></img>
                                                    </div>
                                                    <div className='review_content'>
                                                        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                                        <h3>Alex Rony</h3>
                                                        <h4>Web Designer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-md-4'>
                                                <div className='card review'>
                                                    <img className="d-block w-100 position-relative" src={green} alt="Slide 1" />
                                                    <div className='card review_logo'>
                                                        <p>Logo</p>
                                                        <img src={quotes}></img>
                                                    </div>
                                                    <div className='review_content'>
                                                        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                                        <h3>Alex Rony</h3>
                                                        <h4>Web Designer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-md-4'>
                                                <div className='card review'>
                                                    <img className="d-block w-100 position-relative" src={orange} alt="Slide 1" />
                                                    <div className='card review_logo'>
                                                        <p>Logo</p>
                                                        <img src={quotes}></img>
                                                    </div>
                                                    <div className='review_content'>
                                                        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                                        <h3>Alex Rony</h3>
                                                        <h4>Web Designer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Carousel.Item>
                                    <Carousel.Item>
                                        <div className='row mt-5'>
                                            <div className='col-md-4'>
                                                <div className='card review'>
                                                    <img className="d-block w-100 position-relative" src={black} alt="Slide 1" />
                                                    <div className='card review_logo'>
                                                        <p>Logo</p>
                                                        <img src={quotes}></img>
                                                    </div>
                                                    <div className='review_content'>
                                                        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                                        <h3>Alex Rony</h3>
                                                        <h4>Web Designer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-md-4'>
                                                <div className='card review'>
                                                    <img className="d-block w-100 position-relative" src={green} alt="Slide 1" />
                                                    <div className='card review_logo'>
                                                        <p>Logo</p>
                                                        <img src={quotes}></img>
                                                    </div>
                                                    <div className='review_content'>
                                                        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                                        <h3>Alex Rony</h3>
                                                        <h4>Web Designer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-md-4'>
                                                <div className='card review'>
                                                    <img className="d-block w-100 position-relative" src={orange} alt="Slide 1" />
                                                    <div className='card review_logo'>
                                                        <p>Logo</p>
                                                        <img src={quotes}></img>
                                                    </div>
                                                    <div className='review_content'>
                                                        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit.</p>
                                                        <h3>Alex Rony</h3>
                                                        <h4>Web Designer</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Carousel.Item>
                                </Carousel>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div className='container'>
                <div className='form_background'>
                    <div className='row'>
                        <div className='col-md-6'>
                            <div className="rectangle-box">
                                <h2>Talk to us</h2>
                                <p>How may we help you !</p>
                                <form>
                                    <div className='form_align'>
                                        <div className="form-group">
                                            <label>Name</label>
                                            <input type="text" name="input1" placeholder='Name' />
                                        </div>
                                        <div className="form-right-group">
                                            <label>Email</label>
                                            <input type="text" name="input2" placeholder='Email' />
                                        </div>
                                    </div>
                                    <div className='form_right_align'>
                                        <div className="form-group">
                                            <label>Company Name</label>
                                            <input type="text" name="input3" placeholder='Company Name' />
                                        </div>
                                        <div className="form-right-group">
                                            <label>Phone Number</label>
                                            <input type="text" name="input4" placeholder='Phone Number' />
                                        </div>
                                    </div>
                                    <div className="form-message-group">
                                        <label>Message</label>
                                        <textarea type="text" name="input5" placeholder='Write Message' />
                                    </div>
                                    <button type="submit" className='submit_btn'>Send Message</button>
                                </form>
                            </div>
                        </div>
                        <div className='col-md-6'>
                            <div className='contact_information'>
                                <img src={white_web} className='web_img'></img>
                                <h2>Contact Information</h2>
                                <div className='phone_icon'>
                                    <img src={phone} className='phone_img'></img><span>+91 7418304195</span>
                                </div>
                                <div className='envelope_icon'>
                                    <img src={mail} className='envelope_img'></img><span>Info@hysas.com</span>
                                </div>
                                <div className='map_icon'>
                                    <img src={location} className='location_img'></img>
                                    <p>Hysas technologies private Limited, No.61A1 Bolten Puram, 3rd street, Thoothukudi - 628003</p>
                                </div>
                                <p>Hysas technologies private Limited, No.17/2 Visalatchi Nagar, 2nd street,Ekkaduthangal Chennai - 600032.</p>
                                <h3>Follow us on</h3>
                                <div className='social_icon'>
                                    <img src={facebook}></img>
                                    <img src={google}></img>
                                    <img src={linkedin}></img>
                                    <img src={instagram}></img>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
            <div className='chat'>
                <div class="elfsight-app-acd43838-6eea-4b74-8710-8e84ed4fe6be" data-elfsight-app-lazy></div>
            </div>
        </>
    );
}

export default Home;